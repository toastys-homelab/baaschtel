FROM rust:latest as builder

WORKDIR /src

COPY . .

RUN cargo build --release

FROM ubuntu:22.04

COPY --from=builder /src/target/release/baaschtel /usr/local/bin/baaschtel

CMD ["/usr/local/bin/baaschtel"]
