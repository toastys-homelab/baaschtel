use axum::{
    extract::Query,
    routing::{get, post},
    Json, Router,
};
use clap::Parser;
use config::File;
use miette::Diagnostic;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use tracing::info;

#[derive(Debug, Parser)]
pub struct Args {
    listen: Option<String>,
}

#[derive(Debug, Error, Diagnostic)]
pub enum Error {
    #[error(transparent)]
    ConfigError(#[from] config::ConfigError),

    #[error(transparent)]
    AddrParseError(#[from] std::net::AddrParseError),

    #[error(transparent)]
    AxumError(#[from] hyper::Error),
}

type Result<T> = miette::Result<T, Error>;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    listen: String,
}

pub fn load_config(args: Args) -> Result<Config> {
    let cfg = config::Config::builder()
        .add_source(File::with_name("/etc/baaschtel.yaml").required(false))
        .set_default("listen", "0.0.0.0:3000")?
        .set_override_option("listen", args.listen)?
        .build()?;

    info!("Loading configuration");

    Ok(cfg.try_deserialize()?)
}

pub async fn listen(cfg: Config) -> Result<()> {
    let router = Router::new()
        .route("/", post(post_root))
        .route("/", get(get_root))
        .route("/healthz", get(health));

    info!("Starting server on: {}", &cfg.listen);
    axum::Server::bind(&cfg.listen.parse()?)
        .serve(router.into_make_service())
        .await?;
    Ok(())
}

#[derive(Deserialize)]
struct RootJsonArgs {
    param: Option<String>,
}

#[derive(Serialize)]
struct RootResponse {
    param: Option<String>,
}

async fn health() -> String {
    String::from("healthy")
}

async fn get_root(Query(args): Query<RootJsonArgs>) -> Json<RootResponse> {
    Json(RootResponse { param: args.param })
}

async fn post_root(Json(args): Json<RootJsonArgs>) -> Json<RootResponse> {
    Json(RootResponse { param: args.param })
}
